# DevOps docker images

[![build status](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/badges/master/build.svg)](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/pipelines)

This repository contains configuration to build the [UIS DevOps docker
images](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/container_registry).
The images are built nightly via GitLabCI and pushed to this repository's Registry.

## Images

The following image types are built. See the README files in each image's
directory for more details.

* [python-alpine](python-alpine/README.md)
* [python-slim](python-slim/README.md)
* [django](python-alpine/README.md)
* [ucam-webauth-proxy](ucam-webauth-proxy/README.md)
* [ucam-shib-proxy](ucam-shib-proxy/README.md)
* [ucam-mellon-proxy](ucam-mellon-proxy/README.md)
* [logan-terraform](logan-terraform/README.md)
* [gcloud-docker](gcloud-docker/README.md)
* [pa11y-dashboard](pa11y-dashboard/README.md)

## Configuration

The [GitLabCI configuration](.gitlab-ci.yml) iterates over the image types
and builds the docker images in each directly. Different image types may have
different versions built. If the job is running on ``master``, the images are
also pushed to the Registry.

## Creating test images

The [GitLabCI configuration](.gitlab.ci.yml) only pushes images to the local
repository if the branch is `master`. To create images while testing, manually
[start a new pipeline](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/pipelines/new),
select the branch to test and set `PUSH_TEST_IMAGES` to non-zero length string.
Images will then be pushed to a `test` subfolder of the
[registry](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/container_registry).
