# Ansible playbook

Packaged Ansible playbook with vault support.

Run via:

```bash
$ export VAULT_PASSWORD="some-password"
$ docker run \
    -t \
    -e VAULT_PASSWORD \
    -v ${SSH_AUTH_SOCK}:/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent \
    -v $PWD:/workspace:ro \
    uisautomation/ansible-playbook:2.6 \
    <ansible-playbook options>...
```

### Ansible Role Fetcher

Additional roles will be added using the [Ansible Role Fetcher](https://gitlab.developers.cam.ac.uk/uis/devops/tools/ansibleroles) if a `/workspace/.ansibleroles.yaml` exists.

## Tags

``ansible-playbook`` is a base Ansible image. Tags: ``2.7``, ``2.8``, ``devel``.
The ``devel`` tag pulls directly from github's Ansible master branch.

## Non VPN deployments (Optional)

As an alternative to setting docker to use VPN to route SSH within containers in Linux,
make use of the jumphost service from UIS to access .private.cam.ac.uk addresses.
To do so, one will first need access to `aperture.uis.cam.ac.uk`, the access can be requested by
email, the requests are made to the servers and storage team (infra-sas@uis.cam.ac.uk).

To enable this option, set `APERTURE_JUMP_HOST` to a non-empty value as an environmental variable,
this will set `$APERTURE_USER=$USER` and update the `ssh/config` file with the appropriate configuration
inside the ansible-playbook container.
If you are using a non University machine that doesn't use your short CRSid as it's username you will
also need to set `APERTURE_USER` explicitly.

The `ssh/config` file is defined by `entrypoint.sh` providing the variables mentioned above are set.
The config file is set to route all *.private.cam.ac.uk hosts through `aperture.uis.cam.ac.uk`.

## Ansible playbook with using APERTURE_JUMP_HOST
To run the ansible playbook using the Aperture jump host, the `$APERTURE_JUMP_HOST` and `$APERTURE_USER` must be
passed as environment variables inside the container as show below.

```bash
$ export VAULT_PASSWORD="some-password"
$ docker run \
    -t \
    -e VAULT_PASSWORD \
    -e APERTURE_USER -e APERTURE_JUMP_HOST \
    -v ${SSH_AUTH_SOCK}:/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent \
    -v $PWD:/workspace:ro \
    uisautomation/ansible-playbook:2.8 \
    <ansible-playbook options>...
```