# Terraform and Helm container

This builds a container from Google's Cloud SDK alpine image, adding terraform and
helm. Also, installs kubectl, and provides a `with-kubeconfig.sh` wrapper script
for using terraform output (by default `kubeconfig_content` but overrided by
TERRAFORM_KUBECONFIG_OUTPUT env) to set appropriate KUBECONFIG environment variable.

Google service account credentials for the terraform admin user should be mounted
at /credentials.json.

It is recommended that you mount a volume at /terraform_data to hold the
terraform data directory.

When running ``terraform init``, modules will be fethed from git repos via
SSH. In order to allow this, the ssh-agent socket should be forwarded.

## Usage

```bash
$ docker run --rm -it \
    -v ${SSH_AUTH_SOCK}:/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent \
    -v terraform_gitlab_data:/terraform_data/ \
    registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/logan-terraform:latest \
    <terraform or with-kubeconfig.sh command>
```

## Logan

This container can be used by [logan](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan)
tool by specifying either of the following in the repo's `.logan.yaml` file:
```
image: registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/logan-terraform`
image: uisautomation/logan-terraform
```
