# Pa11y-Dashboard container

This builds a container to run pa11y and pa11y-dashboard (WCAG accessibility testing tools).

When run, it provides a web interface on port 80 to allow you to create and view pa11y reports.

## Usage

1. Copy the `env.example` file to `env.production`.
2. Edit the MongoDB credentials.
  Note: Clusters created via Atlas (https://cloud.mongodb.com/) have an ip whitelist for connections, if you're getting connection errors make sure you're in the whitelist.

3. Then run:
```bash
$ docker run \
    --env-file env.production \
    -p 8080:80 \
    registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/pa11y-dashboard:latest
```

You should now see the pa11y-dashboard at: http://localhost:8080
