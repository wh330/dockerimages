const mongoConnection = process.env['MONGO_CONNECTION'];
if (!mongoConnection) {
  throw Error('Environment variable "MONGO_CONNECTION" is not set, it should be a Mongo DB 2.x connnection string. eg: mongo://...')
}

module.exports = {
  "port": 80,
  "noindex": true,
  "readonly": false,
  "webservice": {
    "database": mongoConnection,
    "host": "0.0.0.0",
    "port": 3000,
    "cron": "0 30 0 * * *",
    "chromeLaunchConfig": {
      "args": ["--no-sandbox"]
    }
  }
};

// Pa11y-dashboard currently logs the mongodb connection string, this may contain a username/password, so hide that log statement.
// See: https://github.com/pa11y/pa11y-dashboard/issues/273
const originalConsoleLog = console.log
console.log = (...args) => {
  if (args.some(log => /mongo[^\s]*:\/\//.test(log))) {
    // It is only logged once, so reset console.log back to the original version
    console.log = originalConsoleLog;
    console.log("Attempt to log Mongo DB connection string was blocked");
    return;
  }
  originalConsoleLog(...args)
}
