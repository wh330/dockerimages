# Prometheus remote writer container

This builds a container with a compiled copy of the prometheus remote writer built in.
It is a 2 stage build as the remote writer is inside the main prometheus git repo so the remote writer is complied and then copied to a new ubuntu container

When running it listens on port 9201 and writes the metrics received to the graphite server at the GRAPHITE_ADDRESS given. It is intended to be run from a compose/swarm
with a prometheus container so the prometheus container can be linked on a docker network to this container.

## Usage

```bash
$ docker run \
    -t \
    -e GRAPHITE_PREFIX=from-prom. \
    -e GRAPHITE_ADDRESS=metrics.example.com \
    -e GRAPHITE_PORT=2003 \
    -p 9201:9021 \
    registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/prometheus-remote-writer:latest
```
