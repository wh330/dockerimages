# Python Alpine images

This directory contains a Dockerfile for the uisautomation python image. This
image is simply the upstream Python Alpine image with updates applied, various
useful packages installed and a small set of pre-installed Python modules. These
modules are those which take a long time to install or which are common to all
of our products.

## Tags

To see the list of tags available visit the [CI job](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/-/blob/master/.gitlab-ci.yml)

## Usage

Use like the upstream alpine image:

```Dockerfile
FROM uisautomation/python:3.8-alpine

# ...
```
