-r base.txt
django>=2.2.17,<2.3
git+https://gitlab.developers.cam.ac.uk/uis/devops/django/automationcommon.git@master#egg=django-automationcommon
django-ucamlookup>=3.0.3
django-ucamwebauth
gunicorn
whitenoise
brotlipy  # Support faster compression in whitenoise
