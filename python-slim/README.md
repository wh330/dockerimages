# Python Slim images

This directory contains a Dockerfile for the uisautomation python image. This
image is simply the upstream Python Slim image with updates applied.

## Tags

To see the list of tags available visit the [CI job](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/-/blob/master/.gitlab-ci.yml)

## Usage

Use like the upstream slim image:

```Dockerfile
FROM uisautomation/python:3.8-slim

# ...
```
