# Simple Shibboleth Proxy

This container provides a basic Apache configuration which can be used to proxy
another container and protect resources via Shibboleth and Raven. By default resources
may be accessed by any valid Raven user. Optionally resources may be
further constrained by Lookup group.

## Example

```bash
$ docker run --rm -it \
    -e SERVER_NAME=localhost \
    -e BACKEND_URL=http://www.example.com/ \
    -e SITE_ADMIN_EMAIL=example@cam.ac.uk \
    -v shib-keys:/etc/shibboleth/keys \
    -p 8080:80 \
    uisautomation/ucam-shib-proxy
```
>Before the proxy will function, the SAML metadata needs to be added to the Raven metadata application. See the [metadata](#Metadata) section below.

Visiting http://localhost:8080/ results in a Shibboleth protected instance of
http://example.com/.

Access may be further restricted to a given Lookup group. For example, to
restrict a resource to members of the [UIS
group](https://www.lookup.cam.ac.uk/group/uis-members):

```bash
$ docker run --rm -it \
    -e SERVER_NAME=localhost \
    -e BACKEND_URL=http://www.example.com/ \
    -e SITE_ADMIN_EMAIL=example@cam.ac.uk \
    -v shib-keys:/etc/shibboleth/keys \
    -e LOOKUP_GROUP_ID=101611 \
    -p 8080:80 \
    uisautomation/ucam-shib-proxy
```

## Configuration

The following environment variables are used for configuration:

* BACKEND_URL (required) - URL of site to proxy. Example: http://example.com/
* SERVER_NAME (required) - FQDN of site. Used by mod_ucam_webauth module to
    ensure that the user is on the "canonical" site.
* SITE_ADMIN_EMAIL (required) - The email address that will be included in any error messages
* LOOKUP_GROUP_ID (optional) - Lookup group ID to further restrict valid users.
* IDP_METADATA_URL (optional) - Override the metadata URL for the IdP. See blow.

## Replicated deployment

For convenience, the container generates a random key pair used to encrypt the
shibboleth traffic if one is not specified. This keypair should be stored in a docker volume
to prevent needing to redo the metadata every time the container re-spawns. 
For a replicated deployment, you will want to use the same key pair for each replica.

```bash
# Generate the key pair
$ shib-keygen -o $PWD/keys

# Launch the proxy
$ docker run --rm -it \
    -e SERVER_NAME=localhost \
    -e BACKEND_URL=http://www.example.com/ \
    -v $PWD/keys:/etc/shibboleth/keys:ro \
    -p 8080:80 \
    uisautomation/ucam-shib-proxy
```
## Metadata

Once the container is running for the first time visit http://yourhost.cam.ac.uk/Shibboleth.sso/Metadata to download your metadata. Then upload this to https://metadata.raven.cam.ac.uk/

## Building the container

The container is available on Docker hub but may be built explicitly via the
usual ``docker build -t uisautomation/ucam-shib-proxy .`` command issued in
the root of the repository.

## Using a different IdP

The container is configured by default to use
https://shib.raven.cam.ac.uk/shibboleth as the metadata URL for the IdP. To use,
for example, the test instance:

```bash
$ docker run --rm -it \
    -e IDP_METADATA_URL=https://shib3-test.raven.cam.ac.uk/shibboleth \
    -e SERVER_NAME=localhost \
    -e BACKEND_URL=http://www.example.com/ \
    -e SITE_ADMIN_EMAIL=example@cam.ac.uk \
    -v shib-keys:/etc/shibboleth/keys \
    -e LOOKUP_GROUP_ID=101611 \
    -p 8080:80 \
    uisautomation/ucam-shib-proxy
```
